#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Date    : 2019-12-02 15:03:35
# @Author  : Jeff.Sui (215687736@qq.com)
# @Link    : http://example.org
# @Version : $Id$

import os
import paramiko 
import chardet 

stage_list =['JavaSE','JavaEE1','JavaEE2','JavaWeb','web\xc3\x87\xc2\xb0\xc2\xb6\xc3\x8b']

def serverConn():
    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh.connect(hostname='192.168.1.233',port=11584,username='ftp',password='asjyftp')
    return ssh

ssh = serverConn()

stdin,stdout,stderr = ssh.exec_command('ls -A /home/asjy_student_ftp/1923T')
res_list = stdout.readlines()
for i in res_list:
    # print(chardet.detect(i.encode()))
    print(i.encode())
ssh.close()
