
#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Date    : 2019-12-02 17:15:15
# @Author  : Jeff.Sui (215687736@qq.com)
# @Link    : http://example.org
# @Version : $Id$


#
def  hello():
    print("hello world")
    # return "hello world"
#
def hello_again():
    print("welcome")
    hello()

# v0.2 

def hello_again_2(hello):
     print("welcome")
     hello()
     
# hello()
# print(hello.__name__)
# print(hello())
# hello_again()
hello_again_2(hello)