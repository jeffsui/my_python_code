﻿"""
inpect module provides useful functions to
help get information about live objects
such as modules, classes, methods, functions, tracebacks, frame objects,
and code objects
"""
from inspect import getfullargspec
def get_args(func):
    """
    get_args
    """
    return getfullargspec(func).args

def info(name=None,age=None,address=None):
    """
    info
    """
    print(name,age,address)

if __name__ == '__main__':
    info(name='zhangsan',age=18,address='beijing')
    print(get_args(info))