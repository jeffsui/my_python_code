﻿from utils.my_timer_decorator import timer
"""
20231120 代码片段
第一段代码中的 func 函数直接使用了 global_var,这意味着它在每次循环中都需要访问全局变量 global_var，这可能会导致一些性能上的开销，尤其是在循环次数很大时。

而第二段代码中，将 global_var 复制给了 local_var,然后在循环中使用 local_var,避免了每次循环都去访问全局变量，这样可以提高一定的性能。

因此，第二段代码中的 func 函数可能会比第一段代码中的 func 函数执行得更快。
"""
global_var = 10
@timer
def func():
    ans = 0
    for i in range(10000000):
        ans += global_var+i
    return ans
func()
@timer
def func():
    asns = 0
    local_var = global_var
    for i in range(10000000):
        asns+= local_var+ i
    return asns
func()