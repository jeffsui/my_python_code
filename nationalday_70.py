#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Date    : 2019-10-03 06:43:49
# @Author  : Jeff.Sui (215687736@qq.com)
# @Link    : http://jeffsui.github.io
# @Version : $Id$

from turtle import *
setup(600,400,0,0)
bgcolor('red')
fillcolor('yellow')
color('yellow')
speed(2)

#五角星部分
begin_fill()
up()
goto(-280,100)
down()
for i in range(5):
    fd(150)
    rt(144)
end_fill()

#四颗小五角星部分
begin_fill()
up()
goto(-100,180)
setheading(305)
down()
for i in range(5):
    fd(50)
    rt(144)
end_fill()

begin_fill()
up()
goto(-50,110)
setheading(30)
down()
for i in range(5):
    fd(50)
    rt(144)
end_fill()

begin_fill()
up()
goto(-40,50)
setheading(5)
down()
for i in range(5):
    fd(50)
    rt(144)
end_fill()

begin_fill()
up()
goto(-100,10)
setheading(300)
down()
for i in range(5):
    fd(50)
    rt(144)
end_fill()


hideturtle()

done()