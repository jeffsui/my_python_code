"""
user_input = input("Name ")
if user_input:
    print(user_input)
else:
    print("N/A")
使用下面的技巧可以简化这部分的判断
"""
user_input = input("Name ")
name = user_input or 'N/A'
print(name)
