﻿"""
20231102
python list organization
use this tricky skill
it can make the first item to the last position in a list
"""
inv = [
    "Gem",
    "Sward",
    "Shield",
    "Health  Potion"
]
index = inv.index("Gem")
item = inv.pop(index)
inv.append(item)
print(inv)
