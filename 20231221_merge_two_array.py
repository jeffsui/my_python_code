"""
merge two arrays
"""
def merge_arrays(arrayA,arrayB):
    """_summary_
    Merge arrayA and arrayB
    Remove duplicates
    Sort list in ascending order
    Args:
        arrayA (_type_): an array
        arrayB (_type_): another array

    Returns:
        _type_: sorted and remove duplicates merged array
    """
    return sorted(set(arrayA+arrayB))


a = [1,2,3,3,4,5,6]
b = [4,4,5,6,7,8,9]

print(merge_arrays(a,b))



