﻿old = ['a','b','a','c','b','a']
# if You use set ,that result will cant be ordered list
# new =list(set(old))
# then If you create a new list to save not exsist in the new one ,it works.
"""
new = []
for i in old:
    if i not in new:
        new.append(i)
"""
# here is an tricky tips,using dict.fromkeys()
new = list(dict.fromkeys(old).keys())
print(new)